
Thank You for downloading This Product.

PLEASE BE ADVISED that this model is part of a collection. Therfore, the following text refers to the whole collection.


____________________________________________________________________________
----------------GENERAL INFORMATION-----------------------------------------
____________________________________________________________________________


 �March2015

-Product Name: Playground Collection

-By: Hend Z.

-email: contact.vertx3d@gmail.com

-website : www.vertx3d.com

-Twitter: @vertx3d


____________________________________________________________________________
--------------- DESCRIPTION ------------------------------------------------
____________________________________________________________________________

Playground Collection is a set of 6, ready to use, high resolution models. 
The size is based on real world scale.
The .ma and .mb formats contain limitations and "Set driven keys" that will help you manipulate some objects (like the swings etc). The models mainly contain shaders. Some have a bump map applied to them.    

____________________________________________________________________________
--------------- System requirements-----------------------------------------
____________________________________________________________________________

This product was created in Maya 2012 on a PC. It was not tested on other Version of Maya or on a MAC system. 
The main version (.ma and .mb files) comes with V-Ray, mental ray and standard materials. 
The .fbx and .obj formats contain standard materials.


____________________________________________________________________________
--------------- Installation Instructions ----------------------------------
____________________________________________________________________________

Each element is in a separate file so you can import them separately. In addition, there is .mb and .ma files that include all the elements in one single file.
Each formats is placed in a separate folder, go to the desired folder and open or import the file.


____________________________________________________________________________
--------------- USAGE RESTRICTIONS -----------------------------------------
____________________________________________________________________________

You may use this product for commercial or non-commercial purposes, as you please. You may not redistributed or sell it, in whole or in part.


____________________________________________________________________________
--------------- Files Included in the Product ------------------------------
____________________________________________________________________________

/FBX
     -JungleGym.fbx
     -Lisezmoi.txt
     -Metal_Bump.jpg
     -Readme.txt
     -Roundabout.fbx
     -Seesaw.fbx
     -Slide.fbx
     -SwingA.fbx
     -SwingB.fbx
     
 /MA
     -JungleGym_MR.ma
     -JungleGym_STD.ma
     -JungleGym_V-Ray.ma
     -Lisezmoi.txt
     -Metal_Bump.jpg
     -Playground_MR.ma
     -Playground_STD.ma
     -Playground_V-Ray.ma
     -Readme.txt
     -Roundabout_MR.ma
     -Roundabout_STD.ma
     -Roundabout_V-Ray.ma
     -Seesaw_MR.ma
     -Seesaw_STD.ma
     -Seesaw_V-Ray.ma
     -Slide_MR.ma
     -Slide_STD.ma
     -Slide_V-Ray.ma
     -SwingA_MR.ma
     -SwingA_STD.ma
     -SwingA_V-Ray.ma
     -SwingB_MR.ma
     -SwingB_STD.ma
     -SwingB_V-Ray.ma    

 /MB
     -JungleGym_MR.mb
     -JungleGym_STD.mb
     -JungleGym_V-Ray.mb
     -Lisezmoi.txt
     -Metal_Bump.jpg
     -Playground_MR.mb
     -Playground_STD.mb
     -Playground_V-Ray.mb
     -Readme.txt
     -Roundabout_MR.mb
     -Roundabout_STD.mb
     -Roundabout_V-Ray.mb
     -Seesaw_MR.mb
     -Seesaw_STD.mb
     -Seesaw_V-Ray.mb
     -Slide_MR.mb
     -Slide_STD.mb
     -Slide_V-Ray.mb
     -SwingA_MR.mb
     -SwingA_STD.mb
     -SwingA_V-Ray.mb
     -SwingB_MR.mb
     -SwingB_STD.mb
     -SwingB_V-Ray.mb    

 /OBJ
     -JungleGym.mtl
     -JungleGym.obj
     -Lisezmoi.txt
     -Metal_Bump.jpg
     -Readme.txt
     -Roundabout.mtl
     -Roundabout.obj
     -Seesaw.mtl
     -Seesaw.obj
     -Slide.mtl
     -Slide.obj
     -SwingA.mtl
     -SwingA.obj
     -SwingB.mtl
     -SwingB.obj




For feedback, questions, suggestions or if you have a specific request for a model, please feel free to contact me at contact.vertx3d@gmail.com or via my website www.vertx3d.com  