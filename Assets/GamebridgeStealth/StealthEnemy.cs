﻿using UnityEngine;
using System.Collections;


public class StealthEnemy : MonoBehaviour {
    public int scanDirection = 1;
    public int changeDirectionChancePerSecond = 1;
    public Vector3 lastKnownPosition;
    public bool playerSeen;

    float maxDistance = 100f;
	public enum EnemyState {
		Idle,
		Alert,
		Engaged,
		Dead,
		Dazed,
        Pursuit
	}
	
	public EnemyState state = EnemyState.Idle;
	
	// Use this for initialization
	void Start () {
        playerSeen = false;
	}
	
	// Update is called once per frame
	void Update () {
            Sight();
		if (state == EnemyState.Idle)
		{
			IdleUpdate();
		}
        if (state == EnemyState.Pursuit)
        {
            Pursue();
        }

        
	}
	public float idleRotationSpeed = 100f;
	public void IdleUpdate()
	{
        /*if (Random.Range(0,1) > changeDirectionChancePerSecond)
        {
            scanDirection =- scanDirection;
        }*/
		transform.Rotate(Vector3.up * Time.deltaTime * idleRotationSpeed);
        gameObject.GetComponent<Rigidbody>().velocity = transform.forward * Time.deltaTime * 0;

    }
	public GameObject target = null;
	public void Sight()
	{
		
		RaycastHit hit = new RaycastHit();
		LayerMask mask = new LayerMask();
		mask = LayerMask.NameToLayer("Player");

		
		if ( state != EnemyState.Engaged && Physics.Raycast(transform.position,transform.forward, out hit, maxDistance, mask))
		{
            
            if (hit.collider.tag != "Player")
            {
                target = null;
                playerSeen = false;
                state = EnemyState.Idle;
            }
            if (lastKnownPosition != Vector3.zero && hit.collider.tag != "Player")
            {
                state = EnemyState.Pursuit;
            }
            if (hit.collider.tag == "Player")
            {
                target = hit.transform.gameObject;
                playerSeen = true;
                state = EnemyState.Engaged;
            }

			
		}


		
		if ( state == EnemyState.Engaged && Physics.Raycast(transform.position, transform.forward, out hit, maxDistance, mask))
		{
           
            //idleRotationSpeed = 0f;
			gameObject.transform.LookAt(target.transform);
            gameObject.GetComponent<Rigidbody>().velocity = transform.forward * Time.deltaTime  * speed;
           
            if (playerSeen == true)
            {
                lastKnownPosition = target.transform.position;
            }
            if (hit.collider.tag != "Player")
            {
                state = EnemyState.Pursuit;
            }
		}
		// Raycast forwards to see what's in our  center field of view
		
		// Cone raycast forwards to see what's in our peripheral vision
	
        if (state == EnemyState.Pursuit)
        {
            Pursue();
        }
        	
	}

    public void Pursue()

    {
        gameObject.transform.LookAt(lastKnownPosition);
        gameObject.GetComponent<Rigidbody>().velocity = transform.forward * Time.deltaTime * speed;
        if (transform.position == lastKnownPosition)
        {
            gameObject.GetComponent<Rigidbody>().velocity = transform.forward * Time.deltaTime * 0f;
        }
        transform.Rotate(Vector3.up * Time.deltaTime * idleRotationSpeed);
    }
	
	public float speed = 100f;

    public void OnCollisionEnter(Collision other)
    {
        if (other.collider.tag == "Environment")
        {
            
        }
    }

}
