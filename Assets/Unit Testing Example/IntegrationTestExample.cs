﻿using UnityEngine;
using System.Collections;

public class IntegrationTestExample : MonoBehaviour {
	
		public Vector3 playerInitialPosition;
		public GameObject player;
		public bool testResult = false;

	public void TestPlayerIsStanding()
	{
		if (player.transform.position.y + 10 >= playerInitialPosition.y)
		{
						testResult = true;
		}
	}
	// Use this for initialization
	void Start () {
		Debug.Log("Testing");
			player = GameObject.FindGameObjectWithTag("Player");
			playerInitialPosition = player.transform.position;
			TestPlayerIsStanding();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
